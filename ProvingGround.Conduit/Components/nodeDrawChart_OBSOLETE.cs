﻿using GH_IO;
using GH_IO.Serialization;
using Grasshopper;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;


using Rhino;
using Rhino.Collections;
using Rhino.Geometry;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ProvingGround.Conduit.Classes;

namespace ProvingGround.Conduit
{
    public class nodeDrawChart_OBSOLETE : GH_Component
    {
        #region Register Node

        private bool _columnChart;
        private bool _barChart;
        private bool _lineChart;

        /// <summary>
        /// Load Node Template
        /// </summary>
        public nodeDrawChart_OBSOLETE()
            : base("Draw Chart", "HUD Chart", "Draw a chart in the HUD", "Proving Ground", "HUD")
        {
            _columnChart = true;
            _barChart = false;
            _lineChart = false;
            UpdateMessage();
        }

        /// <summary>
        /// Writes the value of "PersistentData" at each save of the GH document
        /// </summary>
        /// <param name="writer"></param>
        /// <returns></returns>
        public override bool Write(GH_IWriter writer)
        {
            writer.SetBoolean("IsColumn", _columnChart);
            writer.SetBoolean("IsBar", _barChart);
            writer.SetBoolean("IsLine", _lineChart);
            return base.Write(writer);
        }

        /// <summary>
        /// Reads the value of "PersistentData" at each loading of the GH document
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public override bool Read(GH_IReader reader)
        {
            reader.TryGetBoolean("IsColumn", ref _columnChart);
            reader.TryGetBoolean("IsBar", ref _barChart);
            reader.TryGetBoolean("IsLine", ref _lineChart);
            UpdateMessage();
            return base.Read(reader);
        }

        protected override void AppendAdditionalComponentMenuItems(System.Windows.Forms.ToolStripDropDown menu)
        {
            Menu_AppendItem(menu, "Column Chart", CheckColumnType, true, _columnChart);
            Menu_AppendItem(menu, "Bar Chart", CheckBarType, true, _barChart);
            Menu_AppendItem(menu, "Line Chart", CheckLineType, true, _lineChart);
        }

        private void CheckColumnType(object sender, EventArgs e)
        {
            if (!_columnChart)
            {
                _columnChart = true;
                _barChart = false;
                _lineChart = false;
                UpdateMessage();
                this.ExpireSolution(true);
            }
        }

        private void CheckBarType(object sender, EventArgs e)
        {
            if (!_barChart)
            {
                _barChart = true;
                _columnChart = false;
                _lineChart = false;
                UpdateMessage();
                this.ExpireSolution(true);
            }
        }

        private void CheckLineType(object sender, EventArgs e)
        {
            if (!_lineChart)
            {
                _lineChart = true;
                _columnChart = false;
                _barChart = false;
                UpdateMessage();
                this.ExpireSolution(true);
            }
        }

        /// <summary>
        /// Function to set message at bottom of component
        /// </summary>
        ///
        private void UpdateMessage()
        {
            base.DestroyIconCache();
            if (_columnChart) this.Message = "Column";
            else if (_barChart) this.Message = "Bar";
            else this.Message = "Line";
        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.hidden; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("357b6395-07d2-43e6-bf9a-13bb43c89f8b"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get 
            {
                if (_columnChart) return Properties.Resources.PG_Conduit_Column;
                else if (_barChart) return Properties.Resources.PG_Conduit_Bar;
                else return Properties.Resources.PG_Conduit_LineChart;
            }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {

            pManager.AddRectangleParameter("Boundary", "Bound", "Boundary for chart", GH_ParamAccess.item);
            pManager.AddNumberParameter("Data", "Data", "DataTree of chart values (one path for each series)", GH_ParamAccess.tree);
            pManager.AddTextParameter("Categories", "Categories", "Optional list of categories", GH_ParamAccess.list); // opt 2
            pManager.AddTextParameter("Category Axis Title", "CatTitle", "Category Axis Title", GH_ParamAccess.item, ""); // opt 3
            pManager.AddTextParameter("Value Axis Title", "ValTitle", "Value Axis Title", GH_ParamAccess.item, ""); // opt 4
            pManager.AddIntervalParameter("Value axis range", "ValueRange", "Optional preset range for min/max values on the Axis", GH_ParamAccess.item); // 5
            pManager.AddNumberParameter("Axis Padding", "AxisPad", "Padding for axis labels as pct of chart dimensions", GH_ParamAccess.item, 0.1); // 6
            pManager.AddNumberParameter("Category Padding", "CatPad", "Padding size for category labels as pct of chart dimensions", GH_ParamAccess.item, 0.1); // 7
            pManager.AddNumberParameter("Category Spacing", "CatSpace", "Spacing between categories", GH_ParamAccess.item, 0.2); // 8
            pManager.AddIntegerParameter("Line Weight", "Thick", "Line Weight for line graph series", GH_ParamAccess.list, new List<int> { 2 }); // 9
            pManager.AddGenericParameter("Chart Font", "Font", "Font for text objects in chart", GH_ParamAccess.item); // opt 10
            pManager.AddGenericParameter("Color Palette", "Palette", "Color palette to use for chart", GH_ParamAccess.item); // opt 11
            pManager.AddGenericParameter("Category Axis Style", "CatStyle", "Curve style settings for category axis", GH_ParamAccess.item); // opt 12
            pManager.AddGenericParameter("Value Axis Style", "ValStyle", "Curve style settings for value axis (default is no axis)", GH_ParamAccess.item); // opt 13

            pManager[1].Optional = true;
            pManager[2].Optional = true;
            pManager[3].Optional = true;
            pManager[4].Optional = true;
            pManager[5].Optional = true;
            pManager[6].Optional = true;
            pManager[7].Optional = true;
            pManager[8].Optional = true;
            pManager[9].Optional = true;
            pManager[10].Optional = true;
            pManager[11].Optional = true;
            pManager[12].Optional = true;
            pManager[13].Optional = true;

        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.Register_GenericParam("HUD Drawing Object", "DrawObjs", "HUD Chart Objects");
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {

            #region get inputs

            Rectangle3d B = new Rectangle3d();
            DA.GetData(0, ref B);

            GH_Structure<GH_Number> D = new GH_Structure<GH_Number>();
            DA.GetDataTree<GH_Number>(1, out D);

            if (!D.Any())
            {
                D = new GH_Structure<GH_Number>();
                D.Append(new GH_Number(0), new GH_Path(0));
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "No data has been added for evaluation");
            }

            List<string> C = new List<string>();
            DA.GetDataList(2, C);

            string CAT = "";
            DA.GetData(3, ref CAT);

            string VAT = "";
            DA.GetData(4, ref VAT);

            Interval m_valueRange = new Interval(0, 0);
            if (!DA.GetData(5, ref m_valueRange)) m_valueRange = new Interval(0, 0);

            double AP = 0;
            DA.GetData(6, ref AP);

            double CP = 0;
            DA.GetData(7, ref CP);

            double CS = 0;
            DA.GetData(8, ref CS);

            List<int> LT = new List<int>();
            DA.GetDataList(9, LT);

            if (LT.Count == 0)
            {
                LT.Add(2);
            }

            clsFontStyle m_chartFont = new clsFontStyle() { styleName = "Default Chart Font", unset = true }; // = new clsFontStyle() { styleName = "Default Chart Font", unset = true };

            if (m_chartFont.unset)
            {
                object ChtFnt = new object();
                DA.GetData(10, ref ChtFnt);

                if (ChtFnt.GetType() == typeof(Grasshopper.Kernel.Types.GH_String))
                {
                    Grasshopper.Kernel.Types.GH_String m_fontName = (Grasshopper.Kernel.Types.GH_String)ChtFnt;
                    if (m_fontName.IsValid)
                    {
                        m_chartFont = new clsFontStyle() { styleName = m_fontName.Value, unset = true };
                    }
                }
                else
                {
                    try
                    {
                        DA.GetData(10, ref m_chartFont);
                    }
                    catch { }
                }
            }

            clsPaletteStyle m_thisPalette = new clsPaletteStyle() { styleName = "Default Palette", unset = true};

            if (m_thisPalette.unset)
            {

                object m_objPalette = new object();
                DA.GetData(11, ref m_objPalette);

                if (m_objPalette.GetType() == typeof(Grasshopper.Kernel.Types.GH_String))
                {
                    Grasshopper.Kernel.Types.GH_String m_paletteName = (Grasshopper.Kernel.Types.GH_String)m_objPalette;
                    if (m_paletteName.IsValid)
                    {
                        m_thisPalette = new clsPaletteStyle() { styleName = m_paletteName.Value, unset = true };
                    }
                }
                else
                {
                    try
                    {
                        DA.GetData(11, ref m_thisPalette);
                    }
                    catch { }
                }
            }

            clsCurveStyle m_catAxis = new clsCurveStyle() { styleName = "Default Axis", unset = true };

            if (m_catAxis.unset)
            {

                object m_objCatAxis = new Object();
                DA.GetData(12, ref m_objCatAxis);

                if (m_objCatAxis.GetType() == typeof(Grasshopper.Kernel.Types.GH_String))
                {
                    Grasshopper.Kernel.Types.GH_String m_catAxisName = (Grasshopper.Kernel.Types.GH_String)m_objCatAxis;
                    if (m_catAxisName.IsValid)
                    {
                        m_catAxis = new clsCurveStyle() { styleName = m_catAxisName.Value, unset = true };
                    }
                }
                else
                {
                    try
                    {
                        DA.GetData(12, ref m_catAxis);
                    }
                    catch { }
                }

            }

            clsCurveStyle m_valAxis = new clsCurveStyle() { styleName = "None", unset = true };

            if (m_valAxis.unset)
            {

                object m_objValAxis = new Object();
                DA.GetData(12, ref m_objValAxis);

                if (m_objValAxis.GetType() == typeof(Grasshopper.Kernel.Types.GH_String))
                {
                    Grasshopper.Kernel.Types.GH_String m_valAxisName = (Grasshopper.Kernel.Types.GH_String)m_objValAxis;
                    if (m_valAxisName.IsValid)
                    {
                        m_valAxis = new clsCurveStyle() { styleName = m_valAxisName.Value, unset = true };
                    }
                }
                else
                {
                    try
                    {
                        DA.GetData(12, ref m_valAxis);
                    }
                    catch { }
                }

            }

            #endregion

            // Main Operations

            GH_Number datapoint = (GH_Number)(D.AllData(true).FirstOrDefault());

            double min = datapoint.Value;
            double max = datapoint.Value;

            int categoryCount = C.Count;
            int seriesCounter = 1;

            foreach (GH_Path thisPath in D.Paths)
            {
                categoryCount = Math.Max(categoryCount, D.get_Branch(thisPath).Count);
                seriesCounter += 1;
            }

            foreach (GH_Number thisValue in D.AllData(true))
            {
                double datavalue = thisValue.Value;
                min = Math.Min(min, datavalue);
                max = Math.Max(max, datavalue);
            }

            if (_lineChart && categoryCount < 2)
            {
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Line charts require at least two categories.");
                return;
            }

            // get the maximum dimension in either the X or Y direction
            double maxXY = Math.Max(B.X.Length, B.Y.Length);
            double minXY = Math.Min(B.X.Length, B.Y.Length);

            double adjX = 0;
            double adjY = 0;

            bool m_rangeSpecified = (m_valueRange.Length > 0);

            // get the interval of the value axis if it is not set
            if (!m_rangeSpecified) m_valueRange = new Interval(min, max);

            double m_intersectValue = min < 0 && !m_rangeSpecified ? m_valueRange.NormalizedParameterAt(0) : 0; // m_valueRange.NormalizedParameterAt(min);

            List<iDrawObject> chartObjects = new List<iDrawObject>();
            
            Plane basePlane = Plane.WorldXY;
            basePlane.Origin = B.Corner(0);

            #region Bar Chart Setup

            if (_barChart)
            {

                // Set the Axis label values
                if (CAT != "")
                {
                    adjX += AP * maxXY;
                }
                if (VAT != "")
                {
                    adjY += AP * maxXY;
                }

                double xBase = B.Corner(0).X + adjX;

                if (VAT != "")
                {
                    chartObjects.Add(new DrawText(new List<string>() { VAT },
                        new Rectangle3d(basePlane, new Point3d(B.Corner(0).X + adjX, B.Corner(0).Y, 0), new Point3d(B.Corner(1).X, B.Corner(0).Y + adjY * 0.9, 0)),
                        0, 0, new List<System.Drawing.Color>(), false, new clsFontStyle[] { m_chartFont }) as iDrawObject);
                }

                // create the Y Axis Label
                if (CAT != "")
                {
                    chartObjects.Add(new DrawText(new List<string>() { CAT },
                        new Rectangle3d(basePlane, new Point3d(B.Corner(0).X, B.Corner(0).Y + adjY, 0), new Point3d(B.Corner(0).X + adjX * 0.9, B.Corner(3).Y, 0)),
                        0, 0, new List<System.Drawing.Color>(), true, new clsFontStyle[] { m_chartFont }) as iDrawObject);
                }

                if (C.Count > 0)
                {
                    adjX += CP * maxXY;
                }

                Interval yRange = new Interval(B.Corner(0).Y + adjY, B.Corner(3).Y);
                Interval xRange = new Interval(B.Corner(0).X + adjX, B.Corner(1).X);

                // create the category axis
                if (m_catAxis.styleName != "None")
                {
                    Line yAxis = new Line(new Point3d(xRange.ParameterAt(m_intersectValue), B.Corner(0).Y + adjY, 0), new Point3d(xRange.ParameterAt(m_intersectValue), B.Corner(3).Y, 0));
                    chartObjects.Add(new DrawLines(new List<Line>() { yAxis }, new List<int>(), new List<Color>(), new clsCurveStyle[] { m_catAxis }, 1.0) as iDrawObject);
                }

                // create the category axis
                if (m_valAxis.styleName != "None")
                {
                    Line xAxis = new Line(new Point3d(B.Corner(0).X + adjX, B.Corner(0).Y + adjY, 0), new Point3d(B.Corner(0).X + adjX, B.Corner(3).Y, 0));
                    chartObjects.Add(new DrawLines(new List<Line>() { xAxis }, new List<int>(), new List<Color>(), new clsCurveStyle[] { m_valAxis }, 1.0) as iDrawObject);
                }

                double yCategorySpace = (CS / (categoryCount + 1.0)) * yRange.Length;
                double yCategoryHeight = ((yRange.Length - (yCategorySpace * (categoryCount + 1))) / categoryCount) / D.Paths.Count;
                double yRunner = yRange.Min + yCategorySpace;

                List<Mesh> m_chartMeshes = new List<Mesh>();
                for (int i = 0; i < D.Paths.Count; i++)
                {
                    m_chartMeshes.Add(new Mesh());
                }

                for (int i = 0; i < categoryCount; i++)
                {
                    double yStart = yRunner;

                    for (int j = 0; j < D.Paths.Count; j++)
                    {

                        double thisValue = 0;
                        if (D.get_Branch(j).Count > i)
                        {
                            thisValue = ((GH_Number)D.get_Branch(j)[i]).Value;
                        }

                        double xSet = xRange.ParameterAt(m_valueRange.NormalizedParameterAt(thisValue));

                        m_chartMeshes[j].Vertices.Add(new Point3d(xRange.ParameterAt(m_intersectValue), yRunner, 0));
                        m_chartMeshes[j].Vertices.Add(new Point3d(xSet, yRunner, 0));
                        m_chartMeshes[j].Vertices.Add(new Point3d(xSet, yRunner + yCategoryHeight, 0));
                        m_chartMeshes[j].Vertices.Add(new Point3d(xRange.ParameterAt(m_intersectValue), yRunner + yCategoryHeight, 0));

                        int k = m_chartMeshes[j].Vertices.Count - 4;

                        m_chartMeshes[j].Faces.AddFace(k, k + 1, k + 2, k + 3);

                        yRunner += yCategoryHeight;
                    }

                    if (C.Count > i)
                    {
                        chartObjects.Add(new DrawText(new List<string>() { C[i] },
                        new Rectangle3d(basePlane, new Point3d(xBase, yStart, 0), new Point3d(xBase + (CP * maxXY * 0.8), yRunner, 0)), 1, 0,
                        new List<Color>(), true, new clsFontStyle[] { m_chartFont }) as iDrawObject);
                    }

                    yRunner += yCategorySpace;
                }

                chartObjects.Add(new DrawMeshGroup(m_chartMeshes, 1.0, m_thisPalette, false));

            #endregion

            }
            else
            {

                #region Column and Line Chart Setup

                // Set the Axis label values
                if (VAT != "")
                {
                    adjX += AP * maxXY;
                }
                if (CAT != "")
                {
                    adjY += AP * maxXY;
                }

                double xAxisAdjust = m_valueRange.NormalizedParameterAt(0) * (B.Corner(3).Y - (B.Corner(0).Y + adjY));

                //create the X Axis Label

                if (CAT != "")
                {
                    chartObjects.Add(new DrawText(new List<string>() { CAT },
                        new Rectangle3d(basePlane, new Point3d(B.Corner(0).X + adjX, B.Corner(0).Y, 0), new Point3d(B.Corner(1).X, B.Corner(0).Y + adjY * 0.8, 0)),
                        0, 0, new List<System.Drawing.Color>(), false, new clsFontStyle[] { m_chartFont }) as iDrawObject);
                }

                if (C.Count > 0)
                {
                    adjY += CP * maxXY;
                }

                // create the Y Axis Label
                if (VAT != "")
                {
                    chartObjects.Add(new DrawText(new List<string>() { VAT },
                        new Rectangle3d(basePlane, new Point3d(B.Corner(0).X, B.Corner(0).Y + adjY, 0), new Point3d(B.Corner(0).X + adjX * 0.8, B.Corner(3).Y, 0)),
                        0, 0, new List<System.Drawing.Color>(), true, new clsFontStyle[] { m_chartFont }) as iDrawObject);
                }

                 // create the value axis

                Interval yRange = new Interval(B.Corner(0).Y + adjY, B.Corner(3).Y);
                Interval xRange = new Interval(B.Corner(0).X + adjX, B.Corner(1).X);

                // create the category axis
                if (m_catAxis.styleName != "None")
                {
                    Line xAxis = new Line(new Point3d(B.Corner(0).X + adjX, yRange.ParameterAt(m_intersectValue), 0), new Point3d(B.Corner(1).X, yRange.ParameterAt(m_intersectValue), 0));
                    chartObjects.Add(new DrawLines(new List<Line>() { xAxis }, new List<int>(), new List<Color>(), new clsCurveStyle[] { m_catAxis }, 1.0) as iDrawObject);
                }

                // create the category axis
                if (m_valAxis.styleName != "None")
                {
                    Line yAxis = new Line(new Point3d(B.Corner(0).X + adjX, B.Corner(0).Y + adjY, 0), new Point3d(B.Corner(0).X + adjX, B.Corner(3).Y, 0));
                    chartObjects.Add(new DrawLines(new List<Line>() { yAxis }, new List<int>(), new List<Color>(), new clsCurveStyle[] { m_valAxis }, 1.0) as iDrawObject);
                }

                double yBase = B.Corner(0).Y + adjY;

                if (_columnChart)
                {

                    double xCategorySpace = (CS / (categoryCount + 1.0)) * xRange.Length;
                    double xCategoryWidth = ((xRange.Length - (xCategorySpace * (categoryCount + 1))) / categoryCount) / D.Paths.Count;
                    double xRunner = xRange.Min + xCategorySpace;

                    List<Mesh> m_chartMeshes = new List<Mesh>();
                    for (int i = 0; i < D.Paths.Count; i++)
                    {
                        m_chartMeshes.Add(new Mesh());
                    }

                    for (int i = 0; i < categoryCount; i++)
                    {
                        double xStart = xRunner;

                        for (int j = 0; j < D.Paths.Count; j++)
                        {

                            double thisValue = 0;
                            if (D.get_Branch(j).Count > i)
                            {
                                thisValue = ((GH_Number)D.get_Branch(j)[i]).Value;
                            }

                            double ySet = yRange.ParameterAt(m_valueRange.NormalizedParameterAt(thisValue));

                            m_chartMeshes[j].Vertices.Add(new Point3d(xRunner, yRange.ParameterAt(m_intersectValue), 0));
                            m_chartMeshes[j].Vertices.Add(new Point3d(xRunner, ySet, 0));
                            m_chartMeshes[j].Vertices.Add(new Point3d(xRunner + xCategoryWidth, ySet, 0));
                            m_chartMeshes[j].Vertices.Add(new Point3d(xRunner + xCategoryWidth, yRange.ParameterAt(m_intersectValue), 0));

                            int k = m_chartMeshes[j].Vertices.Count - 4;

                            m_chartMeshes[j].Faces.AddFace(k, k + 1, k + 2, k + 3);

                            xRunner += xCategoryWidth;
                        }

                        if (C.Count > i)
                        {
                            chartObjects.Add(new DrawText(new List<string>() { C[i] },
                            new Rectangle3d(basePlane, new Point3d(xStart, yBase - CP * maxXY, 0), new Point3d(xRunner, yBase, 0)), 1, 0,
                            new List<Color>(), false, new clsFontStyle[] { m_chartFont }) as iDrawObject);
                        }

                        xRunner += xCategorySpace;
                    }

                    chartObjects.Add(new DrawMeshGroup(m_chartMeshes, 1.0, m_thisPalette, false));

                }
                else if (_lineChart)
                {

                    List<List<Point3d>> m_chartPoints = new List<List<Point3d>>();
                    double xCategoryWidth = (xRange.Length / categoryCount);
                    double xCatHalf = xCategoryWidth * 0.5;
                    double xRunner = xRange.Min + xCatHalf;

                    for (int i = 0; i < D.Paths.Count; i++)
                    {
                        m_chartPoints.Add(new List<Point3d>());
                    }

                    for (int i = 0; i < categoryCount; i++)
                    {

                        for (int j = 0; j < D.Paths.Count; j++)
                        {

                            double thisValue = 0;
                            if (D.get_Branch(j).Count > i)
                            {
                                thisValue = ((GH_Number)D.get_Branch(j)[i]).Value;
                            }

                            double ySet = yRange.ParameterAt(m_valueRange.NormalizedParameterAt(thisValue));

                            m_chartPoints[j].Add(new Point3d(xRunner, ySet, 0));

                        }

                        if (C.Count > i)
                        {
                            chartObjects.Add(new DrawText(new List<string>() { C[i] },
                            new Rectangle3d(basePlane, new Point3d(xRunner - xCatHalf, yBase - CP * maxXY, 0), new Point3d(xRunner + xCatHalf, yBase, 0)), 1, 0,
                            new List<Color>(), false, new clsFontStyle[] { m_chartFont }) as iDrawObject);
                        }

                        xRunner += xCategoryWidth;
                    }

                    var m_lineList = new List<List<Line>>();
                    var m_weightList = new List<int>();

                    for (int i = 0; i < m_chartPoints.Count; i++)
                    {
                        m_lineList.Add(new List<Line>());
                        m_weightList.Add(i < LT.Count
                            ? LT[i]
                            : LT.Last());

                        for (int j = 0; j < m_chartPoints[i].Count - 1; j++)
                        {
                            m_lineList[i].Add(new Line(m_chartPoints[i][j], m_chartPoints[i][j + 1]));
                        }
                    }

                    chartObjects.Add(new DrawLineGroup(m_lineList, m_weightList, new clsPaletteStyle[] { m_thisPalette }));

                }

                #endregion

            }

            DA.SetDataList(0, chartObjects);
        }
        #endregion
    }
}

